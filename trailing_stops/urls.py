from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
import trailstop_app.views
admin.autodiscover()

urlpatterns = patterns('',

    url(r'^admin/', include(admin.site.urls)),
    url(r'^login/$', 'django.contrib.auth.views.login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout'),
    # url(r'^/$', include('trailstop_app.urls')),
    url(r'^$', trailstop_app.views.index, name='index'),
    url(r'^contacts/(?P<pk>\d+)/$', trailstop_app.views.ContactView.as_view(),
        name='contacts-view',),
    url(r'^contacts/new$', trailstop_app.views.CreateContactView.as_view(),
        name='contacts-new',),
    url(r'^contacts/edit/(?P<pk>\d+)/$', trailstop_app.views.UpdateContactView.as_view(),
        name='contacts-edit',),
    url(r'^contacts/delete/(?P<pk>\d+)/$', trailstop_app.views.DeleteContactView.as_view(),
        name='contacts-delete',),
    url(r'^contacts/$', trailstop_app.views.ListContactView.as_view(),
        name='contacts-list',),
)
urlpatterns += staticfiles_urlpatterns()
