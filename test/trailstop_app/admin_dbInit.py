from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException, NoAlertPresentException
import unittest

class AdminViewsInitPython(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = "http://127.0.0.1:8000/admin/"
        self.media_url = "http://127.0.0.1:8000/media/"
        self.verificationErrors = []
        self.accept_next_alert = True
        self.user = "forrest"
        self.passwd = "P@ssw0rd"
    
    def go_home_app(self):
        driver = self.driver
        driver.find_element_by_link_text("Home").click()
        driver.find_element_by_link_text("Trailstop_app").click()
    
    def user_login(self):
        driver = self.driver
        driver.get(self.base_url)
        driver.find_element_by_id("id_username").clear()
        driver.find_element_by_id("id_username").send_keys(self.user)
        driver.find_element_by_id("id_password").clear()
        driver.find_element_by_id("id_password").send_keys(self.passwd)
        driver.find_element_by_css_selector("input[type=\"submit\"]").click()
        driver.find_element_by_link_text("Trailstop_app").click()
        
    def add_Contact(self, name, phone, email, page=None):
        self.go_home_app()
        driver = self.driver
        driver.find_element_by_link_text("Contacts").click()
        driver.find_element_by_link_text("Add contact").click()
        driver.find_element_by_id("id_name").clear()
        driver.find_element_by_id("id_name").send_keys(name)
        driver.find_element_by_id("id_phone").clear()
        driver.find_element_by_id("id_phone").send_keys(phone)
        driver.find_element_by_id("id_email").clear()
        driver.find_element_by_id("id_email").send_keys(email)
        driver.find_element_by_id("id_page").clear()
        driver.find_element_by_id("id_page").send_keys(page)
        driver.find_element_by_name("_save").click()
        self.assertIn('was added successfully', 
                      driver.find_element_by_class_name('success').text,
                      'Contact name="%s"'%name)
        pass
        
    def add_View(self, name, imagesource):
        self.go_home_app()
        driver = self.driver
        driver.find_element_by_css_selector("tr.model-view > td > a.addlink").click()
        driver.find_element_by_id("id_name").clear()
        driver.find_element_by_id("id_name").send_keys(name)
        Select(driver.find_element_by_id("id_imagesource")).select_by_visible_text(imagesource)
        driver.find_element_by_name("_save").click()
        self.assertIn('was added successfully', 
                      driver.find_element_by_class_name('success').text,
                      'View name="%s"'%name)
        pass
        
    def add_ImageMap(self, viewname, name, width, height, imagesource, device=None):
        self.go_home_app()
        driver = self.driver
        driver.find_element_by_css_selector("tr.model-imagemap > td > a.addlink").click()
        Select(driver.find_element_by_id("id_view")).select_by_visible_text(viewname)
        Select(driver.find_element_by_id("id_imagesource")).select_by_visible_text(imagesource)
        driver.find_element_by_id("id_name").clear()
        driver.find_element_by_id("id_name").send_keys(name)
        driver.find_element_by_id("id_width").clear()
        driver.find_element_by_id("id_width").send_keys(width)
        driver.find_element_by_id("id_height").clear()
        driver.find_element_by_id("id_height").send_keys(height)
        if device:
            Select(driver.find_element_by_id("id_device")).select_by_visible_text(device)
        driver.find_element_by_name("_save").click()
        self.assertIn('was added successfully', 
                      driver.find_element_by_class_name('success').text,
                      'ImageMap name="%s"'%name)
        
    def add_users(self):
        self.add_Contact("Forrest Johnston", '415-497-1011', 'forrest@forrestjohnston.com')
        self.add_Contact("Charles Schwab and Company", '415-497-1011', 'forrest@forrestjohnston.com')
        pass
    
    def test_admin_views_init_python(self):
        driver = self.driver
        self.user_login()
        self.add_users()
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
