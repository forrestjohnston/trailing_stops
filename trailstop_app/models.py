from django.db import models
from decimal import Decimal
from trailstop_app.appcfg import Const
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse

"""CurrencyField see: 
http://stackoverflow.com/questions/2013835/django-how-should-i-store-a-money-value
"""
class CurrencyField(models.DecimalField):
    __metaclass__ = models.SubfieldBase

    def to_python(self, value):
        try:
            return super(CurrencyField, self).to_python(value).quantize(Decimal("0.0001"))
        except AttributeError:
            return None
    
class Security(models.Model):
    symbol = models.CharField(max_length=12) # "IBM", "ABST.TO"
    name   = models.CharField(max_length=79) # "International Business M..."
    exchange = models.CharField(max_length=12) # "IBM", "ABST.TO"
    def __unicode__(self):
        return self.symbol
    
class Option(models.Model):
    symbol = models.CharField(max_length=12) # "IBM", "ABST.TO"
    name   = models.CharField(max_length=79) # "International Business M..."
    def __unicode__(self):
        return self.symbol

class UserProfile(models.Model):
    # This line is required. Links UserProfile to a User model instance.
    user = models.OneToOneField(User)
    # The additional attributes we wish to include.
    website = models.URLField(blank=True)
    picture = models.ImageField(upload_to='profile_images', blank=True)

    # Override the __unicode__() method to return out something meaningful!
    def __unicode__(self):
        return self.user.username
       
class Contact(models.Model):
    first_name = models.CharField(max_length=80, )
    last_name = models.CharField(max_length=80)
    email = models.EmailField(unique=True)
    phone = models.CharField(max_length=60, blank=True)
    url = models.URLField(blank=True)
    class Meta:
        unique_together = (("first_name", "last_name", "email"),)
    def __unicode__(self):
        space = ' ' if self.first_name != '' and self.last_name != '' else ''
        return space.join([ self.first_name, self.last_name, ])
    def get_absolute_url(self):
        return reverse('contacts-view', kwargs={'pk': self.id})
    
class Institution(models.Model):
    company = models.CharField(max_length=60)
    contact = models.ForeignKey(Contact)
    def __unicode__(self):
        return self.company
    
class AccountHolder(models.Model):
    contact = models.ForeignKey(Contact)

class Portfolio(models.Model):
    name = models.CharField(max_length=60)
    description = models.CharField(max_length=60)
    holders = models.ManyToManyField(AccountHolder)
    def __unicode__(self):
        return self.name
    
class Account(models.Model):
    name = models.CharField(max_length=60)
    type  = models.CharField(max_length=12,
                             choices=Const.ACCOUNT_TYPE_CHOICES,
                             default=Const.BROKERAGE) # Long, Short
    account_number = models.CharField(max_length=60, blank=True)
    routing_number = models.CharField(max_length=60, blank=True)
    customer_id = models.CharField(max_length=60, blank=True)
    #holder = models.ForeignKey(User, related_name='game_user', verbose_name='Owner'))
    institution = models.ForeignKey(Institution)
    def __unicode__(self):
        return self.name
    
class Holding(models.Model):
    security = models.ForeignKey(Security)
    account = models.ForeignKey(Account)
    shares = models.DecimalField(max_digits=12, decimal_places=4) # 121.125
    cost = models.DecimalField(max_digits=12, decimal_places=4) # 17.25
    type  = models.CharField(max_length=12,
                             choices=Const.HOLDING_TYPE_CHOICES,
                             default=Const.LONG) # Long, Short
    portfolios = models.ManyToManyField(Portfolio)
    def __unicode__(self):
        return self.security.name
    
'''
class StockQuote(models.Model):
    self.symbol        = symbol         # IBM
    self.price         = Decimal(price) # 195.19
    # remaining properties are in sort order
    self.ask           = None   # 198.00
    self.bid           = None   # 194.20
    self.bookValue     = None   # 21.616
    self.change        = None   # -0.49
    self.exchange      = None   # NYSE
    self.date          = None   # 4/11/2014 4:01pm
    self.movAvg200     = None   # 188.639
    self.movAvg50      = None   # 188.639
    self.open          = None   # 195.04
    self.peRatio       = None   # 13.10
    self.pegRatio      = None   # 1.21
    self.percentChange = None   # -0.25 (that's 1/4 percent)
    self.prevClose     = None   # 195.68
    self.priceBook     = None   # 9.05
    self.priceSales    = None   # 2.04
    self.shortRatio    = None   # 5.40
    self.volume        = None   # 4835214
    self.yearHigh      = None   # 212.00
    self.yearLow       = None   # 172.19
'''  
