#!/usr/bin/python
# Filename: appConfig.py

def singleton(cls):
    obj = cls()
    cls.__new__ = lambda cls: obj  # Always return the same object
    return cls

@singleton
class Const(object):
    # Holding type(s)
    LONG  = 'Long'
    SHORT = 'Short'
    HOLDING_TYPE_CHOICES = (
        (LONG, LONG),
        (SHORT, SHORT),
    )
    # Account type(s)
    CHECKING = 'Checking'
    SAVINGS = 'Savings'
    BROKERAGE = 'Brokerage'
    RETIREMENT = 'Retirement'
    ACCOUNT_TYPE_CHOICES = (
        (BROKERAGE, BROKERAGE),
        (RETIREMENT, RETIREMENT),
        (CHECKING, CHECKING),
        (SAVINGS, SAVINGS),
    )
    

    