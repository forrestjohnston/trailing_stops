from django.test import TestCase
from trailstop_app.models import Contact
from django.core.exceptions import ValidationError

class ContactTests(TestCase):
    first_name_valid = 'John'
    last_name_valid = 'Smith'
    email_valid = 'john@johnsmith.com'
    
    """Contact model tests."""
    def test_first_last(self):
        contact = Contact(first_name='John', last_name='Smith')
        self.assertEquals( str(contact), 'John Smith', )
        contact = Contact(first_name='John')
        self.assertEquals( str(contact), 'John', )
        contact = Contact(last_name='Smith')
        self.assertEquals( str(contact), 'Smith', )
        
    def test_email(self):
        contact = Contact(first_name='John', last_name='Smith', email='john@johnsmith.com')
        # valid email
        self.assertEquals( str(contact.email), 'john@johnsmith.com', )
        contact.full_clean()
        # invalid, ill-formed email
        contact.email = 'bogus.email.com'
        self.assertRaises(ValidationError, contact.full_clean)
        # invalid, Null email
        contact.email = None
        self.assertRaises(ValidationError, contact.full_clean)
        # invalid, empty email
        contact.email = ''
        self.assertRaises(ValidationError, contact.full_clean)

