from django.shortcuts import render
from django.http import Http404, HttpResponse
from trailstop_app.models import Holding, Contact
from django.core.urlresolvers import reverse
from django.views.generic import CreateView
from django.views.generic import ListView
from django.views.generic import UpdateView
from django.views.generic import DeleteView
from django.views.generic import DetailView
import json
JSONMime = 'application/json'

def to_json(dictobj, pretty=False):
    rval = None
    if pretty:
        rval = json.dumps(dictobj, default=lambda o: o.__dict__, 
                          separators=(', ',': '), sort_keys=True, indent=4)
    else:
        rval = json.dumps(dictobj, default=lambda o: o.__dict__, 
                          separators=(',',':'))
    return rval

def index(request):
    context = None
    return render(request, 'index.html', context)

class ListContactView(ListView):
    model = Contact
    template_name = 'contact_list.html'

class ContactView(DetailView):
    model = Contact
    template_name = 'contact.html'
    
class CreateContactView(CreateView):
    model = Contact
    template_name = 'contact_edit.html'
    
    def get_success_url(self):
        return reverse('contacts-list')

    def get_context_data(self, **kwargs):
        context = super(CreateContactView, self).get_context_data(**kwargs)
        context['action'] = reverse('contacts-new')
        return context
    
class UpdateContactView(UpdateView):
    model = Contact
    template_name = 'contact_edit.html'

    def get_success_url(self):
        return reverse('contacts-list')

    def get_context_data(self, **kwargs):

        context = super(UpdateContactView, self).get_context_data(**kwargs)
        context['action'] = reverse('contacts-edit',
                                    kwargs={'pk': self.get_object().id})
        return context

class DeleteContactView(DeleteView):
    model = Contact
    template_name = 'contact_delete.html'

    def get_success_url(self):
        return reverse('contacts-list')
    
def holdings(request):
    holdings = Holding.objects.order_by('-name')
    if request.META['HTTP_ACCEPT'] == JSONMime:
        holdings_d = [ob.to_dict() for ob in holdings]
        result = { 'holdings': holdings_d, }
        return HttpResponse(to_json(result, True), content_type=JSONMime)
    else:
        context = {'holdings_list': holdings}
        return render(request, 'trailstop_app/holdings.html', context)