from django.contrib import admin
from trailstop_app.models import Security, Option, Institution, UserProfile
from trailstop_app.models import Contact, Portfolio, Account, Holding

for mod in (Security, Option, Institution, UserProfile,
            Contact, Portfolio, Account, Holding):
    admin.site.register(mod)
